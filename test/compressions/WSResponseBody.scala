package compressions

/**
  * Created by mwisniewska on 07.03.18.
  */
case class WSResponseBody() {

  val r = new scala.util.Random

  val randomResponseBody: String = responseBodyWithFixedLettersNo(4 + r.nextInt(8))

  def responseBodyWithFixedLettersNo(lettersNo: Int): String = (1 to lettersNo).map(
    _ => r.alphanumeric.dropWhile(_.isDigit).take(100 + r.nextInt(900))
  ).mkString.toUpperCase

}
