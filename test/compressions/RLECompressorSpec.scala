package compressions

import data.compressions.RLECompressor
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by mwisniewska on 08.03.18.
  */
class RLECompressorSpec extends FlatSpec with Matchers {

  it should "compress and decompress strings" in {
    val rleCompressor = new RLECompressor()
    val exampleString = WSResponseBody().randomResponseBody
    val encoded = rleCompressor.compress(exampleString)
    val decoded = rleCompressor.decompress(encoded)
    decoded.mkString shouldEqual exampleString
  }

  it should "group repeated letters" in {
    val rleCompressor = new RLECompressor()
    val stringWith4Groups = "WWWWMMMKL"
    val encoded = rleCompressor.compress(stringWith4Groups)
    encoded.length shouldBe 4
  }
}
