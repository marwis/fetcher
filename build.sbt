name := "CarjumpChallenge"

version := "1.0"

lazy val `carjumpchallenge` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"


libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies += "org.mockito" % "mockito-core" % "1.9.5" % Test

libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.0.6" % Test

libraryDependencies += "de.leanovate.play-mockws" %% "play-mockws" % "2.4.2" % Test

libraryDependencies ++= Seq( jdbc , cache , ws)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"  