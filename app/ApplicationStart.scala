import javax.inject.{Inject, Singleton}

import data.Scheduler
import play.api.inject.ApplicationLifecycle


/**
  * Created by mwisniewska on 05.03.18.
  */
@Singleton
class ApplicationStart @Inject()(lifecycle: ApplicationLifecycle, scheduler: Scheduler) {
  scheduler.scheduleFetching()
}
