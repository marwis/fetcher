package data.compressions

/**
  * Created by mwisniewska on 05.03.18.
  */
trait Compressor {
  def compress[A]: Seq[A] => Seq[Compressed[A]]
  def decompress[A]: Seq[Compressed[A]] => Seq[A]
}