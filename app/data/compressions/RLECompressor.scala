package data.compressions

/**
  * Created by mwisniewska on 05.03.18.
  */
class RLECompressor extends Compressor {

  override def decompress[A]: (Seq[Compressed[A]]) => Seq[A] = {
    input: Seq[Compressed[A]] => {
      def expand(res: Seq[A], rem: Seq[Compressed[A]]): Seq[A] = {
        if (rem.isEmpty)
          res
        else
          rem.head match {
            case s: Single[A] => expand(res:+s.element, rem.tail)
            case r: Repeat[A] => expand(res++Seq.fill(r.count)(r.element), rem.tail)
          }
      }

      expand(Seq(), input)
    }
  }

  override def compress[A]: (Seq[A]) => Seq[Compressed[A]] = {
    input: Seq[A] => {
      def compress(res: Seq[Compressed[A]], rem: Seq[A]): Seq[Compressed[A]] = rem match {
        case Nil => res
        case seq => {
          val (s, r) = rem span { _ == rem.head }
          compress(res:+createElement(s.length, s.head), r)
        }
      }

      compress(Seq(), input)
    }
  }

  def createElement[A](len: Int, subset: A): Compressed[A] = {
    if (len > 1)
      Repeat(len, subset)
    else
      Single(subset)
  }
}
