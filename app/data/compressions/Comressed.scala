package data.compressions

/**
  * Created by mwisniewska on 05.03.18.
  */
sealed trait Compressed[+A]
case class Single[A](element: A) extends Compressed[A]
case class Repeat[A](count: Int, element: A) extends Compressed[A]