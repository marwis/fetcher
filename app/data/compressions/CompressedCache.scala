package data.compressions

/**
  * Created by mwisniewska on 07.03.18.
  */

case class CompressedCache[A] (implicit compressor: Compressor) {

  private var content: Option[Seq[Compressed[A]]] = None

  def add(input: Seq[A]): Unit =
    content = Some(compressor.compress(input))

  def get(index: Int): Option[A] = {
    content match {
      case Some(seq) => compressor.decompress(seq).lift(index)
      case _ => None
    }
  }

  def getLen: Int = {
    content match {
      case Some(seq) => seq.length
      case _ => 0
    }
  }

}
