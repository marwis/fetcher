package data

import javax.inject.{Inject, Named}

import akka.actor.{ActorRef, ActorSystem}
import data.actors.Fetcher.FetchRequest
import play.Configuration

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
  * Created by mwisniewska on 05.03.18.
  */
class Scheduler @Inject() (actorSystem: ActorSystem, @Named("fetcher") fetcher: ActorRef, config: Configuration)(implicit executionContext: ExecutionContext) {

    def scheduleFetching() = {
        val interval = config.getInt("fetching.interval").toInt.seconds
        actorSystem.scheduler.schedule(initialDelay = 0.milliseconds, interval = interval, receiver = fetcher, message = FetchRequest())
    }
}
