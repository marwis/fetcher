package data.actors

import javax.inject.{Inject, Named}

import akka.actor.{Actor, ActorLogging, ActorRef}
import data.actors.CacheManager.Update
import data.actors.Fetcher.FetchRequest
import play.api.libs.ws._
import akka.pattern.pipe

import scala.concurrent.ExecutionContext

/**
  * Created by mwisniewska on 05.03.18.
  */
class Fetcher @Inject ()(wSClient: WSClient, @Named("cacheManager") cacheManager: ActorRef)(implicit executionContext: ExecutionContext) extends Actor with ActorLogging {

  override def receive: Receive = {
    case FetchRequest() =>
      log.info("Fetching data")
      pipe(wSClient.url("http://challenge.carjump.net/A").get()) to self

    case response: WSResponse =>
      log.info("Fetching completed. Parsing result...")
      cacheManager ! Update(response.body.toArray.filter(_ != '\n'))
  }

}

object Fetcher {

  final case class FetchRequest()

}
