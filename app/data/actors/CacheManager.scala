package data.actors

import akka.actor.{Actor, ActorLogging}
import data.actors.CacheManager.{CachedDataByIndex, GetByIndex, Update}
import data.compressions.{CompressedCache, RLECompressor}

/**
  * Created by mwisniewska on 05.03.18.
  */

class CacheManager extends Actor with ActorLogging  {

  implicit val compressor = new RLECompressor()

  private val cache = CompressedCache[Any]

  override def receive: Receive = {
    case Update(newCacheValue) =>
      log.info("Cache updating")
      cache.add(newCacheValue)
      log.info(s"New cache size ${cache.getLen}")

    case GetByIndex(index) =>
      log.info(s"Getting element with index: $index")
      sender() ! CachedDataByIndex({
        cache.get(index)
      })
  }
}

object CacheManager {

  final case class Update(newCacheValue: Seq[_])
  final case class GetByIndex(index: Int)
  final case class CachedDataByIndex(value: Option[_])

}
