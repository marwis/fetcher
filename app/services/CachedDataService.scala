package services

import javax.inject.{Inject, Named, Singleton}

import akka.actor.ActorRef
import data.actors.CacheManager.{CachedDataByIndex, GetByIndex}
import akka.pattern.ask
import akka.util.Timeout
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.mvc.Results._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by mwisniewska on 05.03.18.
  */

@Singleton
class CachedDataService @Inject()(@Named("cacheManager") cacher: ActorRef)(implicit executionContext: ExecutionContext) {

  implicit val duration: Timeout = 20.seconds

  def getDataAtIndex(index: Int): Future[Result] = {
    (cacher ? GetByIndex(index)).mapTo[CachedDataByIndex].flatMap(
      _.value match {
        case Some(value) => Future.successful(Ok(value.toString))
        case None => Future.successful(BadRequest("Element for given index was not found"))
      }
    )
  }
}
