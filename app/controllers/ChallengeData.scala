package controllers

import javax.inject.Inject

import play.api.mvc._
import services.CachedDataService

class ChallengeData @Inject()(cachedDataService: CachedDataService) extends Controller {

  def getIndex(index: Int) = Action.async {
    cachedDataService.getDataAtIndex(index)
  }
}