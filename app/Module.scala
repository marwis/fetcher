import com.google.inject.AbstractModule
import data.actors.{CacheManager, Fetcher}
import play.api.libs.concurrent.AkkaGuiceSupport
/**
  * Created by mwisniewska on 05.03.18.
  */

class Module extends AbstractModule with AkkaGuiceSupport {
  override def configure() = {
    bindActor[Fetcher]("fetcher")
    bindActor[CacheManager]("cacheManager")
    bind(classOf[ApplicationStart]).asEagerSingleton()
  }
}